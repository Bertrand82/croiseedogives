
import BgThreeScene from './BgThreeScene';
import './App.css';




function App() {
  return (
    <div className="App">
      <header className="App-header">

        <h2>Croisée d'ogives</h2>
      </header>

      <div>
        <section className='bgText'>

          <div>Cette application permet de définir les valeurs caractéristiques d'une croisée d'ogives (voute de type gothique).</div>
          <div>Les  paramètres d'entrée sont : côté a, côté b, épaisseur des arrêtes.</div>
          <div>Les sorties sont les differents rayons de courbure et centres de courbure des coffrages.</div>
          <div>Le nombre total de briques et le prix global sont évalués également .</div>
          <div><img src='./image_parpaing.png' height="40px"></img>Briques: parpaing CREUX 200X200X500 NF Dim. : 50 x 20 cm, soit 10 pièces par m². 0€71  TTC/LA PIÈCE </div>
          <div><img src='./image_siporex.jpg' height="40px"></img> Briques: siporex  Béton cellulaire Ytong 62,5 x 25 x ép. 20 cm  5,90 € TTC/LA PIÈCE et  6,4 carreaux par m².</div>
          <div>Les sources sont disponibles sur <a href="https://gitlab.com/Bertrand82/croiseedogives">gitlab</a></div>
        </section>
       
        <BgThreeScene />
       
          
        
      </div>


    </div>
  );
}

export default App;
