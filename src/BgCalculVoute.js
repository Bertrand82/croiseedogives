import React, { Component } from 'react';
import BgComponent from './BgComponent';

class BgCalculVoute extends Component {
    constructor(props) {
        super(props);

        const initialState = {
            cote_a: 501,
            cote_b: 502,
            e_nervure: 10,
            titre: ' Croisée d\'ogive ',
            briques: [{
                nameBrique: 'Siporex',// Siporex
                nbBriqueNervureParMetre: 5,
                prixUnitaireBriqueNervure: 2.8,
                nbBriqueVoutinParMetre2: 6.5,
                prixUnitaireBriqueVoutin: 8,
            },
            {
                nameBrique: 'Parpaing',// Moellon
                nbBriqueNervureParMetre: 5,
                prixUnitaireBriqueNervure: 1,
                nbBriqueVoutinParMetre2: 10,
                prixUnitaireBriqueVoutin: 1,
            },
            {
                nameBrique: 'Parpaing et Siporex',// Siporex/Moellon
                nbBriqueNervureParMetre: 5,
                prixUnitaireBriqueNervure: 2.8,
                nbBriqueVoutinParMetre2: 10,
                prixUnitaireBriqueVoutin: 1,
            }],


        };

        this.state = {
            data: initialState,
        };
    }

    calculDiagonale(data) {
        var dia2 = data.cote_a * data.cote_a + data.cote_b * data.cote_b;
        var dia = Math.sqrt(dia2);
        return Number.parseFloat(dia).toFixed(0);
    }
    calculHauteurExtrados(data) {
        var h = data.diagonale / 2;
        return h;
    }

    calculCentre(cote, hauteur) {
        var a = cote / 2;
        var rTierPoint = (a * a + hauteur * hauteur) / (2 * a);
        return Number.parseFloat(rTierPoint).toFixed(0);
    }
    calculRayon(centre, e) {
        var r = centre - e;
        return r;
    }

    calculRayonDiagonale(data) {
        return data.diagonale / 2 - data.e_nervure;
    }

    calculLongueurTotaleNervureArreteCote(hauteur, centre, rayon) {
        var alpha = Math.asin(hauteur / centre);
        var l = 2 * alpha * rayon;
        return l;
    }

    calculLongueurTotaleNervureDiagonale(data) {
        return data.rayonDiagonale * Math.PI;
    }

    calculLongueurArreteTotal = function (data) {

        var l = 0;
        l += 2 * data.longueurTotaleArreteCote_a;
        l += 2 * data.longueurTotaleArreteCote_b;
        l += 2 * data.longueurTotaleArreteDiagonale;
        return Number.parseFloat(l).toFixed(0);;
    }

    calculSurfaceTotaleVoutins(data) {
        var l_a = data.longueurTotaleArreteCote_a;
        var l_b = data.longueurTotaleArreteCote_b;
        var s = 0;
        s += l_a * data.cote_b / 2;
        s += l_b * data.cote_a / 2;
        return Number.parseFloat(s).toFixed(0);
    }

    calculNbTotalBriquesNervures(data) {
        data.briques.forEach((b) => {
            var n = data.longueurTotaleArrete * b.nbBriqueNervureParMetre / 100;
            b.nbTotalBriquesNervure = Number.parseFloat(n).toFixed(0);
        })

    }

    calculNbTotalBriquesVoutins(data) {//nbTotalBriquesVoutins
        data.briques.forEach((b) => {
            var n = data.surfaceTotaleVoutins * b.nbBriqueVoutinParMetre2 / 100 / 100;
            b.nbTotalBriquesVoutins = Number.parseFloat(n).toFixed(0);
        })
    }
    calculPrixTotalBriquesNervures(data) {
        data.briques.forEach((b) => {
            var n = data.longueurTotaleArrete * b.nbBriqueNervureParMetre / 100;
            var nbBriques = b.nbTotalBriquesNervure;
            var total = nbBriques * b.prixUnitaireBriqueNervure;
            b.prixTotalBriquesNervure = Number.parseFloat(total).toFixed(0);
        })
    }
    calculPrixTotalBriquesVoutins(data) {
        data.briques.forEach((b) => {
            var n = data.longueurTotaleArrete * b.nbBriqueNervureParMetre / 100;
            var nbBriques = b.nbTotalBriquesVoutins;
            var total = nbBriques * b.prixUnitaireBriqueVoutin;
            b.prixTotalBriquesVoutins = Number.parseFloat(total).toFixed(0);
         })
    }
    calculPrixTotal(data) {
        data.briques.forEach((b) => {
            var total = b.prixTotalBriquesNervures + b.prixTotalBriquesVoutins;
            b.prixTotalBriquesT = Number(b.prixTotalBriquesVoutins) + Number(b.prixTotalBriquesNervure)
        })
    }
    calculVoute(data) {
        data.diagonale = this.calculDiagonale(data);
        data.hauteurExtrados = this.calculHauteurExtrados(data);
        data.centre_a = this.calculCentre(data.cote_a, data.hauteurExtrados);
        data.centre_b = this.calculCentre(data.cote_b, data.hauteurExtrados);
        data.rayon_a = this.calculRayon(data.centre_a, data.e_nervure);
        data.rayon_b = this.calculRayon(data.centre_b, data.e_nervure);
        data.rayonDiagonale = this.calculRayonDiagonale(data);
        data.longueurTotaleArreteCote_a = this.calculLongueurTotaleNervureArreteCote(data.hauteurExtrados, data.centre_a, data.rayon_a)
        data.longueurTotaleArreteCote_b = this.calculLongueurTotaleNervureArreteCote(data.hauteurExtrados, data.centre_b, data.rayon_b)
        data.longueurTotaleArreteDiagonale = this.calculLongueurTotaleNervureDiagonale(data);
        data.longueurTotaleArrete = this.calculLongueurArreteTotal(data);
        data.surfaceTotaleVoutins = this.calculSurfaceTotaleVoutins(data);
        this.calculNbTotalBriquesNervures(data);
        this.calculPrixTotalBriquesNervures(data);
        this.calculNbTotalBriquesVoutins(data);
        this.calculPrixTotalBriquesVoutins(data);
        this.calculPrixTotal(data);
        return data;
    }

    updateParam = (d) => {
        console.log("updateParam2 ----- a: " + d.cote_a + "  b: " + d.cote_b + "  e: " + d.e_nervure);
     
        this.setState({ data: d });
        this.props.updateParam(this.state.data.cote_a, this.state.data.cote_b, this.state.data.e_nervure);
        ;
    }


    getRenderParam(param) {
        const r = []
        this.state.data.briques.forEach((b) => r.push(<td key={'key_' + param + '_' + b.nameBrique}>{b[param]}</td>))
        return r
    }



    render() {
        var data = this.calculVoute(this.state.data);
       
        return (

            <section>

                <table border="1">
                    <thead><tr><td>Paramètres Géométriques</td></tr></thead>
                    <tbody>
                        <tr>
                            <td>titre:</td><td> {data.titre}</td><td></td>
                        </tr>
                        <tr>
                            <td>Coté a:</td>
                            <td> {data.cote_a}</td>
                            <td>Longueur d'un coté en cm (extrados)</td>
                        </tr>
                        <tr>
                            <td>Coté b:</td>
                            <td> {data.cote_b}</td>
                            <td>Longueur de l'autre coté en cm (extrados) </td>
                        </tr>
                        <tr>
                            <td>Epaisseur Nervure:</td>
                            <td> {data.e_nervure}</td>
                            <td>Epaisseur des nervures en cm</td>
                        </tr>
                        <tr>
                            <td>Diagonale :</td>
                            <td> {data.diagonale}</td>
                            <td>Longueur de la diagonale (extrados)</td>
                        </tr>
                        <tr>
                            <td>Hauteur :</td><td> {data.hauteurExtrados}</td>
                            <td>Hauteur (extrados)</td>
                        </tr>
                        <tr>
                            <td>Position centre a : </td>
                            <td> {data.centre_a}</td>
                            <td>Permet de dessiner le coffrage du coté a . (voir rayon plus bas)</td>
                        </tr>
                        <tr>
                            <td>Position centre b :</td><td>  {data.centre_b}</td><td></td>
                        </tr>
                        <tr>
                            <td>rayon  a:</td>
                            <td>{data.rayon_a}</td>
                            <td>Permet de dessiner le coffrage du coté a . (Avec la position du centre a)</td>
                        </tr>
                        <tr>
                            <td>rayon  b:</td>
                            <td>{data.rayon_b}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>rayon  diagonale:</td>
                            <td>{data.rayonDiagonale}</td>
                            <td>Permet de dessiner le coffrage de la croisee</td>
                        </tr>

                        <tr>
                            <td>Longueur totale arrêtes:</td>
                            <td>{data.longueurTotaleArrete}</td>
                            <td>Permet d'estimer le nombre de briques necessaires pour les arrêtes</td>
                        </tr>
                        <tr>
                            <td>Surface totale voutins:</td>
                            <td>{data.surfaceTotaleVoutins}</td>
                            <td>Permet d'estimer le nombre de briques necessaires pour la constructions des voutins</td>
                        </tr>
                    </tbody>

                </table>
                <table border="1">
                    <thead><tr><td>Comparaison par type de brique</td></tr></thead>
                    <tbody>
                        <tr>
                            <td>Brique </td>
                            {this.getRenderParam('nameBrique')}
                            <td></td>
                        </tr>
                        <tr><td>Nervures :</td></tr>
                        <tr>
                            <td> Nombre de briques / m:</td>
                            {this.getRenderParam('nbBriqueNervureParMetre')}
                            <td></td>
                        </tr>
                        <tr>
                            <td>Prix unitaire brique nervure:</td>
                            {this.getRenderParam('prixUnitaireBriqueNervure')}
                            <td>Euros</td>
                        </tr>
                        <tr>
                            <td>Nb total de briques nervure:</td>
                            {this.getRenderParam('nbTotalBriquesNervure')}
                            <td></td>
                        </tr>
                        <tr>
                            <td>Prix total  briques nervure:</td>
                            {this.getRenderParam('prixTotalBriquesNervure')}
                            <td>Euros</td>
                        </tr>
                       
                        <tr><td>Voutins</td></tr>
                        <tr>
                            <td>Nombre de briques / m2:</td>
                            {this.getRenderParam('nbBriqueVoutinParMetre2')}
                            <td></td>
                        </tr>
                        <tr>
                            <td>Nb total de briques voutins:</td>
                            {this.getRenderParam('nbTotalBriquesVoutins')}
                            <td></td>
                        </tr>
                        <tr>
                            <td>Prix unitaire brique voutin:</td>
                            {this.getRenderParam('prixUnitaireBriqueVoutin')}
                            <td>Euros</td>
                        </tr>
                        <tr>
                            <td>Prix total  briques voutins:</td>
                            {this.getRenderParam('prixTotalBriquesVoutins')}
                            <td>Euros</td>
                        </tr>
                        <tr><td>Total</td></tr>
                        <tr>
                            <td>prixTotalBriques:</td>
                            {this.getRenderParam('prixTotalBriquesT')}
                            <td>Euros</td>
                        </tr>


                    </tbody>

                </table>
                <BgComponent updateParam={this.updateParam} data={this.state.data} />

            </section>
        );

    }

}
export default BgCalculVoute;