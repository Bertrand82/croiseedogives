
import './indexReactDataGui.css';


import DatGui, {
  DatNumber,
  DatString,
  DatButton,
  DatFolder,
  DatSelect,
} from 'react-dat-gui';
import React, { Component } from 'react';
import { brique_1, brique_2, brique_3 } from './BgCalculVoute';
/**
 * Demonstrates presets that extend the default preset (initial state)
 * as well as presets which extend the current state
 */
class BgComponent extends Component {
  constructor(props) {
    super(props);



    this.state = {
      data: props.data,
      defaultData: props.data,
    };
  }


  // Update current state with changes from controls
  handleUpdate = newData => {
    console.log("handleUpdate2", this.state);
    console.log("handleUpdate2", this.state);
    this.setState(prevState => ({
      data: { ...prevState.data, ...newData }
    })
    );
    // Update parent
    this.props.updateParam(this.state.data);
    this.getDatFolder = this.getDatFolder.bind(this);
  }

  getDatFolder(b) {
    
    return (
      <DatFolder key={'key_' + b.nameBrique} title={b.nameBrique}>



        <DatFolder title="Brique Nervures">

          <DatNumber
            path="nbBriqueNervureParMetre"
            label="Nb Briques / metre"
            min={2}
            max={50}
            step={0.1}
          />
          <DatNumber
            path="prixUnitaireBriqueNervure"
            label="Prix Unitaire "
            min={0.5}
            max={20}
            step={0.1}
          />

        </DatFolder>

        <DatFolder title="Briques Voutins">
          <DatNumber
            path="nbBriqueVoutinParMetre2"
            label="Nb Briques / m2"
            min={2}
            max={100}
            step={0.1}
          />
          <DatNumber
            path="prixUnitaireBriqueVoutin"
            label="Prix Unitaire "
            min={0.5}
            max={20}
            step={0.1}
          />

        </DatFolder>
      </DatFolder>);

  }

  render() {
    const { data, defaultData } = this.state;
    const briquesData = [];
    if (this.state.data.briques) {
       this.state.data.briques.forEach(b => {
        briquesData.push(this.getDatFolder(b))
      });
    }
    return (
      <main style={{ marginRight: '350px' }}>

        <DatGui data={data} onUpdate={this.handleUpdate}>

          <DatNumber
            path="cote_a"
            label="Coté a (cm)"
            min={100}
            max={900}
            step={1}
          />
          <DatNumber
            path="cote_b"
            label="Coté b (cm)"
            min={100}
            max={900}
            step={1}
          />
          <DatNumber
            path="e_nervure"
            label="e_nervure (cm)"
            min={1}
            max={50}
            step={1}
          />
          {briquesData}
        </DatGui>
      </main>
    );
  }
}

export default BgComponent;