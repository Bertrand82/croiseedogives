
Ce projet permet de visualiser en 3D une croisee d'ogives gothique.
Les entrées sont (au minimum) la largeur et la longueur de la croisée , les sorties sont le nombre de briques consommées , les rayons de courbures des arc, et même le prix des materiaux (Sous réserve que le prix unitaire de la brique utilisée est consistant)

Il est visible : https://croiseedogives.firebaseapp.com/
Il est hébergé sur gitlab : https://gitlab.com/Bertrand82/croiseedogives


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Pour installer three:

npx create-react-app  croiseedogives

install three:
npm i three
npm i three react-three-fiber
npm install react-dat-gui --save












